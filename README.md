**Snake Game**
==========

Commit categories
-------------
- **[INITIAL]** — for the initial commit and set up
- **[ADD]** — broad category for code that’s added
- **[UPDATE]** — for small updates
- **[REFACTOR]** — refactored code
- **[FIX**] — fixes (e.g. typos, linter rules, broken links/imports, etc.)
- **[TESTS]** — if you write them :)
- **[STYLES]** — style related commits
- **[BUILD]** — during the build process
- **[PRODUCTION]** — production related
- **[WIP]** — work in progress
- **[REMOVE]** — when removing files or old, unnecessary code

MANUAL
-------------
- **Run all**
```
$> npm start
```
Run all rules below. Same terminal for both output.
- **Run HTTP Server**
```
$> npm run webserver
```
Run the web server.
- **Run the build app**
```
$> npm run babel-gulp
```
Build all the app assets. Only client side JS for the moment.