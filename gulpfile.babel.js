var gulp = require('gulp');
var browserify = require('browserify');
var babelify = require('babelify');
var replace = require('browserify-replace');
var source = require('vinyl-source-stream');

const SRC_PATH = './src/js/index.js';
const DST_PATH = './public/assets/js';
const BUNDLE_NAME = 'bundle.js';

gulp.task('build', function () {
    return browserify({entries: SRC_PATH, extensions: ['.js'], debug: false})
        .transform(babelify)
        .bundle()
        .pipe(source(BUNDLE_NAME))
        .pipe(gulp.dest(DST_PATH));
});

gulp.task('watch', ['build'], function () {
    gulp.watch('src/js/**/*.js', ['build']);
});

gulp.task('default', ['watch']);