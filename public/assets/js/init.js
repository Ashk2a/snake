
function initFood() {
    food = new PIXI.Sprite(foodTexture);
    food.height = config.foodSize;
    food.width = config.foodSize;
    food.x = (Math.floor(Math.random() * Math.floor(config.gridSize)) % config.gridSize) * config.blockSize + ((config.blockSize - config.foodSize) / 2);;
    food.y =  (Math.floor(Math.random() * Math.floor(config.gridSize)) % config.gridSize) * config.blockSize + ((config.blockSize - config.foodSize) / 2);;
    food.vx = 0;
    food.vy = 0;
    container.addChild(food);
}

function initGrid() {
    for (var i = 0; i < Math.pow(config.gridSize, 2); i++) {
        var block = new PIXI.Sprite(blockTexture);
        block.height = config.blockSize;
        block.width = config.blockSize;
        block.x = (i % config.gridSize) * config.blockSize;
        block.y = Math.floor(i / config.gridSize) * config.blockSize;
        container.addChild(block);
    }
}

function initSnake() {
    var point = new PIXI.Sprite(headSnakeTexture);
    point.height = config.snakePointSize;
    point.width = config.snakePointSize;
    point.x = (0 % config.gridSize) * config.blockSize + ((config.blockSize - config.snakePointSize) / 2);
    point.y = Math.floor(0 / config.gridSize) + ((config.blockSize - config.snakePointSize) / 2);
    point.vx = 0;
    point.vy = 0;
    snake.push(point);
    container.addChild(point);

}
function addSnake(x, y) {
    var point = new PIXI.Sprite(snakeTexture);
    point.height = config.snakePointSize;
    point.width = config.snakePointSize;
    point.x = x;
    point.y = y;
    point.vx = 0;
    point.vy = 0;
    snake.unshift(point);
    container.addChild(point);

}