var app = new PIXI.Application(800, 600, {backgroundColor: 0x1099bb});
document.body.appendChild(app.view);
var containerMenu = new PIXI.Container();
app.stage.addChild(containerMenu);

var configMenu = {
    'buttonHeight' : 100,
    'buttonWidth' : 300
};
let messageMenu = new PIXI.Text("");
var buttonTexture = PIXI.Texture.fromImage('assets/img/buttonStart.png');
var buttonStart = new PIXI.Sprite(buttonTexture);
function initMenu(){
    messageMenu.text =  "Menu";
    messageMenu.position.set(configMenu.buttonWidth/2-messageMenu.width/2,-configMenu.buttonHeight);
    containerMenu.addChild(messageMenu);
    buttonStart.height = configMenu.buttonHeight;
    buttonStart.width = configMenu.buttonWidth;
    buttonStart.anchor.set(0.1);
    buttonStart.x = containerMenu.width/2;
    buttonStart.y = containerMenu.height/2;
    // Opt-in to interactivity
    buttonStart.interactive = true;
    // Shows hand cursor
    buttonStart.buttonMode = true;
    buttonStart
        .on('pointerdown', onClick)
        .on('pointerover', onButtonOver)
        .on('pointerout', onButtonOut);
    containerMenu.addChild(buttonStart);

}
function onClick () {
    containerMenu.removeChild(buttonStart);
    init();
}
function onButtonOver() {
    buttonStart.scale.x *= 1.05;
    buttonStart.scale.y *= 1.05;
}
function onButtonOut() {
    buttonStart.scale.x /= 1.05;
    buttonStart.scale.y /= 1.05;
}
initMenu();
containerMenu.x = (app.screen.width - containerMenu.width) / 2;
containerMenu.y = (app.screen.height - containerMenu.height) / 2;