var container = new PIXI.Container();

app.stage.addChild(container);

var config = {
    'gridSize': 10,
    'blockSize': 55,
    'snakePointSize': 55,
    'foodSize': 30
};


var blockTexture = PIXI.Texture.fromImage('assets/img/block.png');
var miamTexture = PIXI.Texture.fromImage('assets/img/miam.png');
var snakeTexture = PIXI.Texture.fromImage('assets/img/snake.png');
var foodTexture = PIXI.Texture.fromImage('assets/img/food.png');
var headSnakeTexture = PIXI.Texture.fromImage('assets/img/head.png');

var snake = [];
var state;
var food;
var tick = 0;
var direction = 'ArrowRight';


function keyboard(value) {
    let key = {};
    key.value = value;
    key.isDown = false;
    key.isUp = true;
    key.press = undefined;
    key.release = undefined;
    //The `downHandler`
    key.downHandler = event => {
        if (event.key === key.value) {
            if (key.isUp && key.press) key.press();
            key.isDown = true;
            key.isUp = false;
            event.preventDefault();
        }
    };

    //The `upHandler`
    key.upHandler = event => {
        if (event.key === key.value) {
            if (key.isDown && key.release) key.release();
            key.isDown = false;
            key.isUp = true;
            event.preventDefault();
        }
    };

    //Attach event listeners
    const downListener = key.downHandler.bind(key);
    const upListener = key.upHandler.bind(key);

    window.addEventListener(
        "keydown", downListener, false
    );
    window.addEventListener(
        "keyup", upListener, false
    );

    // Detach event listeners
    key.unsubscribe = () => {
        window.removeEventListener("keydown", downListener);
        window.removeEventListener("keyup", upListener);
    };

    return key;
}
function hitTest(r1, r2) {
    //Define the variables we'll need to calculate
    let hit, combinedHalfWidths, combinedHalfHeights, vx, vy;

    //hit will determine whether there's a collision
    hit = false;

    //Find the center points of each sprite
    r1.centerX = r1.x + r1.width / 2;
    r1.centerY = r1.y + r1.height / 2;
    r2.centerX = r2.x + r2.width / 2;
    r2.centerY = r2.y + r2.height / 2;

    //Find the half-widths and half-heights of each sprite
    r1.halfWidth = r1.width / 2;
    r1.halfHeight = r1.height / 2;
    r2.halfWidth = r2.width / 2;
    r2.halfHeight = r2.height / 2;

    //Calculate the distance vector between the sprites
    vx = r1.centerX - r2.centerX;
    vy = r1.centerY - r2.centerY;

    //Figure out the combined half-widths and half-heights
    combinedHalfWidths = r1.halfWidth + r2.halfWidth;
    combinedHalfHeights = r1.halfHeight + r2.halfHeight;

    //Check for a collision on the x axis
    if (Math.abs(vx) < combinedHalfWidths) {

        //A collision might be occurring. Check for a collision on the y axis
        if (Math.abs(vy) < combinedHalfHeights) {

            //There's definitely a collision happening
            hit = true;
        } else {

            //There's no collision on the y axis
            hit = false;
        }
    } else {

        //There's no collision on the x axis
        hit = false;
    }

    //`hit` will be either `true` or `false`
    return hit;
}
function gameLoop(delta) {
    state(delta);
}

function play(delta) {
    tick += delta;
    if (tick >= 8) {
        if (hitTest(snake[snake.length-1], food)) {
            addSnake(snake[snake.length-1].x, snake[snake.length-1].y);
            container.removeChild(food);
            initFood();
            updatePoints();
        }
        else if(!checkSnakeHit()){
            updatePoints();
            tick = 0;
        }else{
            let message = new PIXI.Text("Loose !");
            app.stage.addChild(message);
            message.position.set(snake[snake.length-1].x, snake[snake.length-1].y);
        }

    }
}

function init() {

    initGrid();
    initSnake();
    initFood();
    let left = keyboard("ArrowLeft"),
        up = keyboard("ArrowUp"),
        right = keyboard("ArrowRight"),
        down = keyboard("ArrowDown");

    //Left arrow key `press` method
    right.press = () => {
        direction = "ArrowRight";
    };
    left.press = () => {
        direction = "ArrowLeft";
    };

    up.press = () => {
        direction = "ArrowUp";
    };
    down.press = () => {
        direction = "ArrowDown";
    };

    state = play;
    app.ticker.add(delta => gameLoop(delta));
    container.x = (app.screen.width - container.width) / 2;
    container.y = (app.screen.height - container.height) / 2;
}

function updatePoint(point, vx, vy) {
    if(point.x + vx == config.gridSize*config.blockSize&& vx == 55) {
        console.log("test")
        point.vx =vx;
        point.vy = vy;
        point.x = 0;
        point.y += vy;
    }
    else if(point.x === 0 && vx == -55) {
        point.vx =vx;
        point.vy = vy;
        point.x = (config.gridSize-1) * config.blockSize + ((config.blockSize - config.snakePointSize) / 2);
        point.y += vy;
    }
    else if(point.y + vy == config.gridSize*config.blockSize&& vy == 55) {
        console.log("test")
        point.vx = vx;
        point.vy = vy;
        point.x += vx;
        point.y = 0;
    }
    else if(point.y === 0 && vy == -55) {

        point.vx = vx;
        point.vy = vy;
        point.x += vx;
        point.y = (config.gridSize-1) * config.blockSize + ((config.blockSize - config.snakePointSize) / 2);
    }
    else {
        point.vx = vx;
        point.vy = vy;
        point.x += vx ;
        point.y +=vy;
    }
}

function updatePoints() {
    snake.forEach(function (element, index) {
        if (index === snake.length - 1) {
            switch (direction) {
                case "ArrowLeft":
                    if(element.vx != 55){
                        updatePoint(element, -config.blockSize, 0);
                    }else{
                        direction = "ArrowRight";
                        updatePoint(element, config.blockSize, 0);
                    }
                    break;
                case "ArrowRight":
                    if(element.vx != -55){
                        updatePoint(element, config.blockSize, 0);
                    }else{
                        direction = "ArrowRight";
                        updatePoint(element, -config.blockSize, 0);
                    }
                    break;
                case "ArrowUp":
                    if(element.vy != 55){
                        updatePoint(element, 0, -config.blockSize);
                    }else{
                        direction = "ArrowDown";
                        updatePoint(element, 0, config.blockSize);
                    }
                    break;
                case "ArrowDown":
                    if(element.vy != -55){
                        updatePoint(element, 0, config.blockSize);
                    }else{
                        direction = "ArrowUp";
                        updatePoint(element, 0, -config.blockSize);
                    }
                    break;
            }
        }
        else {
            element.vx = snake[index + 1].vx;
            element.vy = snake[index + 1].vy;
            element.x = snake[index + 1].x;
            element.y = snake[index + 1].y;
        }
    });
}

function checkSnakeHit(){
    for(var i = 0; i < snake.length -1;i++){
        if(hitTest(snake[snake.length-1],snake[i])){
            return true;
        }
    }
    return false;
}


