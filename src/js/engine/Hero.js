import {app} from "./App";

export class Hero extends PIXI.Sprite {
    constructor(parent = false) {
        super();
        // Init Hero loader resources
        this.loader = new PIXI.loaders.Loader();
        // Add resources
        this.loader.add('hero', 'assets/img/snake.png');
        // Do action on all resources loaded
        this.loader.load((loader, resources) => {
            // Init Hero texture from resource
            this.texture = resources.hero.texture;
            // Init Hero position
            this.x = 0;
            this.y = app.screen.height - this.height;
            // Init Hero velocity
            this.vx = 0;
            this.vy = 0;
        });
        // Add this to parent child if parent is given
        if (parent)
            parent.addChild(this);
        // Init Hero state
        this.isJumping = false;
        this.isFalling = false;
        this.isMoving = {
            'left': false,
            'right': false
        };
        // Set keys listeners
        this.keys = {
            'left': app.keyboard("ArrowLeft"),
            'right': app.keyboard("ArrowRight"),
            'space': app.keyboard(" "),
        };
        // Do actions on keys listener
        this.keysActions();
    }

    keysActions() {
        // LEFT
        this.keys.left.press = () => {
            this.vx = -5;
            this.vy = 0;
            this.isMoving.left = true;
        };
        this.keys.left.release = () => {
            this.vx = 0;
            this.isMoving.left = false;
        };
        // RIGHT
        this.keys.right.press = () => {
            this.vx = 5;
            this.vy = 0;
            this.isMoving.right = true;
        };
        this.keys.right.release = () => {
            this.vx = 0;
            this.isMoving.right = false;
        };
        // SPACE
        this.keys.space.press = () => {
            this.isJumping = true;
        };
    }

    tick(delta) {
        if (this.isJumping) {
            this.vy = -15;
            if (this.y <= app.screen.height - 120) {
                this.isFalling = true;
                this.isJumping = false;
            }
        }
        if (this.isFalling) {
            this.vy = 5;
            if (this.y + this.vy > app.screen.height - this.height)
                this.vy = 0;
            this.isJumping = false;
        }

        if (this.y === app.screen.height - this.height)
            this.isFalling = false;

        this.x += this.vx;
        this.y += this.vy;
    }
}
