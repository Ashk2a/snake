class App extends PIXI.Application {
    constructor(width, height, options, noWebGL, useSharedTicker) {
        super(width, height, options, noWebGL, useSharedTicker);
        document.body.appendChild(this.view);
        this.stages = [];
        this.debug = new PIXI.Text();
        this.debug.x = 30;
        this.debug.y = 10;
    }

    static getInstance(width, height, options, noWebGL, useSharedTicker) {
        if (self.instance === undefined)
            self.instance = new App(width, height, options, noWebGL, useSharedTicker);
        return self.instance;
    }

    keyboard(value) {
        let key = {};
        key.value = value;
        key.isDown = false;
        key.isUp = true;
        key.press = undefined;
        key.release = undefined;
        //The `downHandler`
        key.downHandler = event => {
            if (event.key === key.value) {
                if (key.isUp && key.press) key.press();
                key.isDown = true;
                key.isUp = false;
                event.preventDefault();
            }
        };

        //The `upHandler`
        key.upHandler = event => {
            if (event.key === key.value) {
                if (key.isDown && key.release) key.release();
                key.isDown = false;
                key.isUp = true;
                event.preventDefault();
            }
        };

        //Attach event listeners
        const downListener = key.downHandler.bind(key);
        const upListener = key.upHandler.bind(key);

        window.addEventListener(
            "keydown", downListener, false
        );
        window.addEventListener(
            "keyup", upListener, false
        );

        // Detach event listeners
        key.unsubscribe = () => {
            window.removeEventListener("keydown", downListener);
            window.removeEventListener("keyup", upListener);
        };

        return key;
    }

    play(state) {
        this.state = state;
        this.ticker.add(delta => state(delta));
    }

    printeDebug(content) {
        this.stage.addChild(this.debug);
        this.debug.text = content;
    }
}

var app = App.getInstance(500, 450, {
        backgroundColor: 0xffCCfA,
    }
);

export {app};