import * as PIXI from 'pixi.js';
import * as Viewport from 'pixi-viewport';
import {app} from './engine/App';
import {Hero} from "./engine/Hero";

var scene = new PIXI.extras.Viewport({
    screenWidth: app.screen.width,
    screenHeight: app.screen.height,
    worldWidth: 850,
    worldHeight: app.screen.height,
    interaction: app.renderer.interaction,
});


var backgroundImage = new PIXI.Texture.fromImage('assets/img/background.jpg'),
    backgroundTexture = new PIXI.Texture(backgroundImage, new PIXI.Rectangle(0, 0, scene.worldWidth, scene.worldHeight)),
    background = new PIXI.Sprite(backgroundTexture);

scene.addChild(background);
var hero = new Hero(scene);


app.stage.addChild(scene);
app.play((delta) => {
    if (hero.x >= scene.screenWidth / 2)
        if (hero.x <= scene.worldWidth - scene.screenWidth / 2)
            scene.moveCorner(hero.x - scene.screenWidth / 2, scene.y);
    hero.tick(delta);
});